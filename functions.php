<?php if (! defined('ABSPATH')) :
	exit; // Exit if accessed directly
endif;

function creptaam_childtheme_scripts(){
	$parent_style = 'creptaam-parent-style'; // This is creptaam parent theme style.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

    wp_enqueue_style( 'creptaam-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action('wp_enqueue_scripts', 'creptaam_childtheme_scripts', 98);

// ehsan
// for svg support
function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

function ehsan_custom_scripts() {
    //wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/ehsan-custom.js', array( 'jquery' ),'',true );
}
add_action( 'wp_enqueue_scripts', 'ehsan_custom_scripts' );


#---Added by TAKEXPERT---#
// sidebar settings
function parsrix_additional_sidebars(){
    register_sidebar( apply_filters( 'creptaam_footer_left_column', array(
            'name'          => esc_html__('Parsrix Footer Sidebar Left Column', 'creptaam'),
            'id'            => 'creptaam-footer-left-column',
            'description'   => esc_html__('Appears in the footer', 'creptaam'),
            'before_widget' => '<div id="%1$s" class="col-md-4 col-sm-6 text-right footer-contact widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )));
    register_sidebar( apply_filters( 'creptaam_footer_right_column', array(
            'name'          => esc_html__('Parsrix Footer Sidebar Right Column', 'creptaam'),
            'id'            => 'creptaam-footer-right-column',
            'description'   => esc_html__('Appears in the footer', 'creptaam'),
            'before_widget' => '<div id="%1$s" class="col-md-4 col-sm-6 text-center widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )));
}
add_action('widgets_init', 'parsrix_additional_sidebars');

// register menu position
function parsrix_register_menus() {
  register_nav_menus(
    array(
      'parsrix-footer-bottom' => __( 'ParsRix Footer Bottom Menu' ),
    )
  );
}
add_action( 'init', 'parsrix_register_menus' );
